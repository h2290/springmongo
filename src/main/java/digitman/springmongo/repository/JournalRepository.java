package digitman.springmongo.repository;

import digitman.springmongo.domain.Journal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface JournalRepository extends MongoRepository<Journal, String> {

    public Journal findByLabel(String label);
}
