package digitman.springmongo.repository;

import digitman.springmongo.domain.FinancialYear;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface FinancialYearRepository extends MongoRepository<FinancialYear, String> {
}
