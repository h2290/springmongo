package digitman.springmongo.repository;

import digitman.springmongo.domain.*;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface AccountRepository extends MongoRepository<Account, String> {
}
