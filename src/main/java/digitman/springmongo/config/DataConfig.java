package digitman.springmongo.config;

import com.mongodb.*;
import com.mongodb.client.*;
import java.util.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

/**
 *
 * @author thomas
 */
@Configuration
public class DataConfig extends AbstractMongoClientConfiguration {

    @Override
    protected String getDatabaseName() {
        return "SpringMongo";
    }

    @Override
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017/" + getDatabaseName());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();
        return MongoClients.create(mongoClientSettings);
    }

    @Override
    public Collection getMappingBasePackages() {
        return Collections.singleton("digitman.springmongo");
    }
}
