package digitman.springmongo.domain;

/**
 *
 * @author thomas
 */
public record Account(String number, String description) {
}
