package digitman.springmongo.domain;

/**
 *
 * @author thomas
 */
public class InvalidJournalEntryException extends RuntimeException {

    public InvalidJournalEntryException(String message) {
        super(message);
    }

    public InvalidJournalEntryException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidJournalEntryException(Throwable cause) {
        super(cause);
    }
}
