package digitman.springmongo.domain;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author thomas
 */
public record JournalEntry(
        LocalDate bookingDate,
        LocalDate storageDate,
        List<JournalTransaction> transactions,
        String comment) {
}
