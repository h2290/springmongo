package digitman.springmongo.domain;

/**
 *
 * @author thomas
 */
public record JournalTransaction(String account, Double amount, String comment) {
}
