package digitman.springmongo.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.time.LocalDate;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

/**
 *
 * @author thomas
 */
public record FinancialYear(
        @Id @JsonSerialize(using = ToStringSerializer.class) ObjectId id,
        LocalDate start,
        LocalDate end,
        List<Journal> journals) {

    public static FinancialYear create(LocalDate start, LocalDate end) {
        return new FinancialYear(null, start, end, List.of());
    }
}
