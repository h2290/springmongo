package digitman.springmongo.domain;

import java.util.*;
import java.util.stream.Stream;
import org.bson.types.ObjectId;

/**
 *
 * @author thomas
 */
public record Journal(ObjectId _id, String label, String name, List<JournalEntry> entries) {

    public static Journal create(String label, String name) {
        return new Journal(null, label, name, List.of());
    }

    public Journal addEntry(JournalEntry entry) {
        if (entry.transactions() == null || entry.transactions().isEmpty()) {
            throw new InvalidJournalEntryException("Transaktioner saknas för mottagen journalpost");
        }
        if (Math.abs(entry.transactions().stream().map(t -> t.amount()).reduce((a, b) -> a + b).get()) > 0.005) {
            throw new InvalidJournalEntryException("Journalposten är inte i balans");
        }
        return new Journal(_id, label, name,
                Stream.concat(Optional.ofNullable(entries)
                        .orElse(List.of()).stream(), Stream.of(entry))
                        .toList());
    }
}
