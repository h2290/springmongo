package digitman.springmongo.service;

import digitman.springmongo.domain.FinancialYear;
import digitman.springmongo.repository.FinancialYearRepository;
import digitman.springmongo.service.exception.InvalidFinancialYearException;
import java.util.*;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class FinancialYearService {

    private final FinancialYearRepository repository;

    @Autowired
    public FinancialYearService(FinancialYearRepository repository) {
        this.repository = repository;
    }

    public List<FinancialYear> all() {
        return repository.findAll();
    }

    public FinancialYear save(FinancialYear financialYear) {
        validateFinancialYear(financialYear);
        return repository.save(financialYear);
    }

    private void validateFinancialYear(FinancialYear fy) {
        Optional<FinancialYear> previousYear = repository.findAll().stream().reduce((a, b) -> a.end().isAfter(b.end()) ? a : b);
        if (fy.start().getDayOfMonth() != 1
                || previousYear.isPresent() && !fy.start().minusDays(1).equals(previousYear.get().end())) {
            throw new InvalidFinancialYearException("Otillåtet startdatum för räkenskapsår");
        }
        if (fy.end().getDayOfMonth() != fy.end().lengthOfMonth()) {
            throw new InvalidFinancialYearException("Otillåtet slutdatum för räkenskapsår");
        }
        if (fy.end().isBefore(fy.start())) {
            throw new InvalidFinancialYearException("Otillåtet intervall för räkenskapsår");
        }
    }

    public FinancialYear byId(String id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException());
    }
}
