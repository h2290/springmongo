package digitman.springmongo.service.exception;

/**
 *
 * @author thomas
 */
public class InvalidFinancialYearException extends RuntimeException {

    public InvalidFinancialYearException(String message) {
        super(message);
    }

    public InvalidFinancialYearException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidFinancialYearException(Throwable cause) {
        super(cause);
    }
}
