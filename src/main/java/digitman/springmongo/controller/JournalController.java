package digitman.springmongo.controller;

import digitman.springmongo.domain.*;
import digitman.springmongo.repository.JournalRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("journal")
public class JournalController {

    private final JournalRepository journalRepository;

    @Autowired
    public JournalController(JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    @PostMapping(path = "{label}/{name}")
    public Journal addJournal(@PathVariable String label, @PathVariable String name) {
        return journalRepository.save(Journal.create(label, name));
    }

    @PostMapping(path = "{label}")
    public JournalEntry addJournalEntry(@PathVariable String label, @RequestBody JournalEntry entry) {
        Journal journal = journalRepository.findByLabel(label);
        Journal result = journal.addEntry(entry);
        journalRepository.save(result);
        return entry;
    }

    @GetMapping
    public List<Journal> listJournals() {
        return journalRepository.findAll();
    }
}
