package digitman.springmongo.controller;

import digitman.springmongo.domain.FinancialYear;
import digitman.springmongo.service.FinancialYearService;
import java.time.LocalDate;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("financialYear")
public class FinancialYearController {

    private final FinancialYearService service;

    @Autowired
    public FinancialYearController(FinancialYearService service) {
        this.service = service;
    }

    @PostMapping(path = "{start}/{end}")
    public FinancialYear add(@PathVariable LocalDate start, @PathVariable LocalDate end) {
        return service.save(FinancialYear.create(start, end));
    }

    @GetMapping
    public List<FinancialYear> all() {
        return service.all();
    }

    @GetMapping(path = "{id}")
    public FinancialYear byId(@PathVariable String id) {
        return service.byId(id);
    }
}
