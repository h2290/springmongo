package digitman.springmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMongo {

    public static void main(String[] args) {
        SpringApplication.run(SpringMongo.class, args);
    }
}
